using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class UI : MonoBehaviour
{

    public GameObject startPanel;
    public Image potatoTimerVisual;
    public Image soldierTimerVisual;

    public TextMeshProUGUI waveText;
    public TextMeshProUGUI medalText;
    public TextMeshProUGUI potatoAmountText;
    public TextMeshProUGUI soldierAmountText;
    public TextMeshProUGUI villagersAmount;

    public float waveTimer;
    public float potatoTimer;
    public float soldierTimer;
    public int villagers;
    public int soldiers;
    public int medals;
    public int potatoAmount = 10;
    int waveCount = 1;
    public int villagerCost;
    public int soldierCost;

    void Start()
    {
        Time.timeScale = 0;
        startPanel.SetActive(true);
        potatoAmountText.text = $"{potatoAmount}";
    }

    public void StartButton()
    {
        Time.timeScale = 1;
        startPanel.SetActive(false);

    }
    
    public void VillagerHireButton()
    {
        if(potatoAmount >= villagerCost)
        {
            potatoAmount -= villagerCost;
            villagers++;
        }
        villagersAmount.text = $"{villagers}";
        potatoAmountText.text = $"{potatoAmount}";
    }

    public void SoldierHireButton()
    {
        if (potatoAmount >= soldierCost)
        {
            potatoAmount -= soldierCost;
            soldiers ++;
        }
        soldierAmountText.text = $"{soldiers}";
        potatoAmountText.text = $"{potatoAmount}";
    }

    private void Update()
    {
        WaveTimer(/*30 sec*/);
        if(villagers > 0)
        {
            PotatoTimer(/*~5 sec*/);
        }
        if(soldiers > 0)
        {
            SoldierTimer(/*15 sec*/);
        }
    }

    private float WaveTimer()
    {
        waveTimer = waveTimer - 1 * Time.deltaTime;
        if(waveTimer <= 0)
        {
            waveTimer = 30;
            waveCount++;
            medals++;
            medalText.text = $"{medals}";
        }
        waveText.text = $"WAVE {waveCount} WILL START IN {waveTimer:0} SECONDS";
        return waveTimer;
    }
    private float PotatoTimer()
    {
        potatoTimer = potatoTimer - 1 * Time.deltaTime;
        if (potatoTimer <= 0)
        {
            potatoTimer = 5;
            potatoAmount += villagers;
            potatoAmountText.text = $"{potatoAmount}";
        }
        potatoTimerVisual.fillAmount = potatoTimer / 5;
        return potatoTimer;
    }

    private float SoldierTimer()
    {
        soldierTimer = soldierTimer - 1 * Time.deltaTime;
        if (soldierTimer <= 0 && potatoAmount >= soldiers)
        {
            soldierTimer = 15;
            potatoAmount -= soldiers;
            potatoAmountText.text = $"{potatoAmount}";
        } else if (soldierTimer <= 0 && potatoAmount < soldiers)
        {
            soldiers--;
            soldierAmountText.text = $"{soldiers}";
        }
        soldierTimerVisual.fillAmount = soldierTimer / 15;
        return soldierTimer;
    }

}
